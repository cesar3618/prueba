﻿using ProyectoBanco.Inicio.Dato;
using ProyectoBanco.Inicio.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoBanco.Inicio
{
    public partial class FrmInicio2 : Form
    {
        frmClaveAleat Datos;
        frmLogin Admin2;
        DataTable table;
        Usuario Dato = new Usuario();
        Conexion C = new Conexion();
        public FrmInicio2(Form f)
        {
            Datos = (frmClaveAleat)f;
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            C.Conectar();
            string Consulta = "insert into Usuario (Nombre,Nombre2,Apellido,Apellido2,Edad,Clave) values ('"+txtNombre.Text+"','"+txtNombre2.Text+"','"+txtApellido.Text+"','"+txtApellido2.Text+"',"+txtEdad.Text+",'"+txtClave.Text+"')";
            C.Ejecutar(Consulta);
            this.ActualizarGrid();
            //Guardar();
            //Iniciar();
            Limpiar();
            //Consultar();
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }
        private void Iniciar()
        {
            table = new DataTable();
            table.Columns.Add("Nombre");
            table.Columns.Add("Nombre 2");
            table.Columns.Add("Apellido");
            table.Columns.Add("Apellido 2");
            table.Columns.Add("Edad");
            table.Columns.Add("Clave");
            Grille.DataSource = table;
        }
        private void Guardar()
        {
            var modelo = new UsuarioModel()
            {
                Nombre = txtNombre.Text,
                Nombre2 = txtNombre2.Text,
                Apellido = txtApellido.Text,
                Apellido2= txtApellido2.Text,
                Edad = int.Parse(txtEdad.Text),
                Clave = txtClave.Text
            };
            Dato.Guardar(modelo);
        }
        /// <summary>
        /// Aqui consultamos los datos requeridos
        /// </summary>
        private void Consultar()
        {
            foreach (var item in Dato.Consultar())
            {
                DataRow fila = table.NewRow();
                fila["Nombre"] = item.Nombre;
                fila["Nombre 2"] = item.Nombre2;
                fila["Apellido"] = item.Apellido;
                fila["apellido 2"] = item.Apellido2;
                fila["Edad"] = item.Edad;
                fila["Clave"] = item.Clave;
                table.Rows.Add(fila);
            }
        }
        private void Limpiar()
        {
            txtNombre.Text = "";
            txtNombre2.Text = "";
            txtApellido.Text = "";
            txtApellido2.Text = "";
            txtEdad.Text = "";
            txtClave.Text = "";

        }

        private void btnAtras_Click(object sender, EventArgs e)
        {

            MessageBox.Show("Volviendo a la anterior ventana");
            Admin2 = new frmLogin();
            Datos.Close();
            Admin2.Show();
            this.Hide();
        }
        public void ActualizarGrid()
        {
            C.ActualizarGrid(this.Grille,"select * from Usuario");
        }

        private void FrmInicio2_Load(object sender, EventArgs e)
        {
            this.ActualizarGrid();
        }
    }
}

