﻿using ProyectoBanco.Inicio.Modelo;
using ProyectoBanco.Login_Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoBanco.Inicio.Dato
{
    public class Usuario
    {
        List<UsuarioModel> lista = new List<UsuarioModel>();
        List<Deposito> lista1 = new List<Deposito>();
        /// <summary>
        /// Guarda a los usuarios
        /// </summary>
        /// <param name="modelo"> datos del usuario/param>
        public void Guardar(UsuarioModel modelo)
        {
            lista.Add(modelo);
        }
        /// <summary>
        /// Consulta todos los ususarios 
        /// </summary>
        /// <returns>datos de usuario</returns>
        public List<UsuarioModel> Consultar()
        {
            return lista;
        }

        internal void Guardar(Deposito deposito)
        {
        }
    }
}
