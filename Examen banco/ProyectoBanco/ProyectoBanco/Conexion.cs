﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ProyectoBanco
{
    class Conexion
    {
        SqlConnection conexion;

        public void Conectar()
        {
            conexion = new SqlConnection("Data Source=DESKTOP-Q42DMLP;Initial Catalog=Banco;Integrated Security=True");
            conexion.Open();
        }

        public void Desconectar()
        {
            conexion.Close();
        }
        public void Ejecutar(String consulta)
        {
            SqlCommand com = new SqlCommand(consulta, conexion);
            int filasAfectadas = com.ExecuteNonQuery();

            if (filasAfectadas > 0)
            {
                MessageBox.Show("Operacion exitosa", "Base de datos modificada", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            else
            {
                MessageBox.Show("no se ha conectado a la base de datos", "Error del sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }
        public void ActualizarGrid(DataGridView dg, string consulta)
        {
            this.Conectar();

            System.Data.DataSet ds = new System.Data.DataSet();

            SqlDataAdapter da = new SqlDataAdapter(consulta, conexion);


            da.Fill(ds, "Usuario");

            dg.DataSource = ds;
            dg.DataMember = "Usuario";

            this.Desconectar();
        }
    }
}
