﻿using ProyectoBanco.Inicio.Dato;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace ProyectoBanco.Login_Usuario
{
    public partial class frmPlataforma : Form
    {
        
        frmLogin2 login2 = new frmLogin2();
        Deposito deposito = new Deposito();
        public frmPlataforma()
        {
            
            InitializeComponent();
        }

        private void Btnatrasplataforma_Click(object sender, EventArgs e)
        {
            this.Close();
            login2.Show();
        }

        private void btnDeposito_Click(object sender, EventArgs e)
        {
            deposito.Show();
        }
        

    }
}
