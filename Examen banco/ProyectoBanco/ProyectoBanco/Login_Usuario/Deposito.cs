﻿using System;
using ProyectoBanco.Inicio.Dato;
using ProyectoBanco.Inicio.Modelo;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoBanco.Login_Usuario
{
    public partial class Deposito : Form
    {
        DataTable table;
        Usuario Dato = new Usuario();
        public Deposito()
        {
            InitializeComponent();
        }

        public string nombre { get; internal set; }
        public string deposito1 { get; internal set; }
        public string clave { get; internal  set; }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Iniciar();
            GuardarDeposito();
            ConsultarDeposto();
            LimpiarDeposito();

        }
        private void Iniciar()
        {
            table = new DataTable();
            table.Columns.Add("Nombre");
            table.Columns.Add("Deposito");
            table.Columns.Add("Clave");
            dataGridView1.DataSource = table;
        }
        private void GuardarDeposito()
        {
            var modelo = new UsuarioModel()
            {
                nombre = txtnombre.Text,
                deposito1 = txtdeposito.Text,
                clave = txtclave.Text
            };
            Dato.Guardar(modelo);
        }
        private void ConsultarDeposto()
        {
            foreach (var item in Dato.Consultar())
            {
                DataRow fila = table.NewRow();
                fila["Nombre"] = item.nombre;
                fila["Deposito"] = item.deposito1;
                fila["Clave"] = item.clave;
                table.Rows.Add(fila);
            }
        }
        private void LimpiarDeposito()
        {
            txtnombre.Text = "";
            txtdeposito.Text = "";
            txtclave.Text = "";

        }
        private void Consultar()
        {
            foreach (var item in Dato.Consultar())
            {
                DataRow fila = table.NewRow();
                fila["Nombre"] = item.Nombre;
                fila["Nombre 2"] = item.Nombre2;
                fila["Apellido"] = item.Apellido;
                fila["apellido 2"] = item.Apellido2;
                fila["Edad"] = item.Edad;
                fila["Clave"] = item.Clave;
                table.Rows.Add(fila);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();

        }
    }
}
