﻿namespace ProyectoBanco.Login_Usuario
{
    partial class frmPlataforma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnInicio = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnIngresos = new System.Windows.Forms.Button();
            this.btnDeposito = new System.Windows.Forms.Button();
            this.Btnatrasplataforma = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnInicio
            // 
            this.btnInicio.Location = new System.Drawing.Point(64, 53);
            this.btnInicio.Margin = new System.Windows.Forms.Padding(4);
            this.btnInicio.Name = "btnInicio";
            this.btnInicio.Size = new System.Drawing.Size(100, 46);
            this.btnInicio.TabIndex = 0;
            this.btnInicio.Text = "Inicio";
            this.btnInicio.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(205, 53);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 46);
            this.button1.TabIndex = 1;
            this.button1.Text = "Avisos";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(95, 16);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Banco_Aztex";
            // 
            // btnIngresos
            // 
            this.btnIngresos.Location = new System.Drawing.Point(64, 128);
            this.btnIngresos.Margin = new System.Windows.Forms.Padding(4);
            this.btnIngresos.Name = "btnIngresos";
            this.btnIngresos.Size = new System.Drawing.Size(100, 58);
            this.btnIngresos.TabIndex = 3;
            this.btnIngresos.Text = "Ingresos";
            this.btnIngresos.UseVisualStyleBackColor = true;
            // 
            // btnDeposito
            // 
            this.btnDeposito.Location = new System.Drawing.Point(64, 212);
            this.btnDeposito.Margin = new System.Windows.Forms.Padding(4);
            this.btnDeposito.Name = "btnDeposito";
            this.btnDeposito.Size = new System.Drawing.Size(100, 58);
            this.btnDeposito.TabIndex = 4;
            this.btnDeposito.Text = "Deposito";
            this.btnDeposito.UseVisualStyleBackColor = true;
            this.btnDeposito.Click += new System.EventHandler(this.btnDeposito_Click);
            // 
            // Btnatrasplataforma
            // 
            this.Btnatrasplataforma.Location = new System.Drawing.Point(205, 128);
            this.Btnatrasplataforma.Name = "Btnatrasplataforma";
            this.Btnatrasplataforma.Size = new System.Drawing.Size(100, 58);
            this.Btnatrasplataforma.TabIndex = 5;
            this.Btnatrasplataforma.Text = "atras";
            this.Btnatrasplataforma.UseVisualStyleBackColor = true;
            this.Btnatrasplataforma.Click += new System.EventHandler(this.Btnatrasplataforma_Click);
            // 
            // frmPlataforma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(403, 404);
            this.Controls.Add(this.Btnatrasplataforma);
            this.Controls.Add(this.btnDeposito);
            this.Controls.Add(this.btnIngresos);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnInicio);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmPlataforma";
            this.Text = "Plataforma";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnInicio;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnIngresos;
        private System.Windows.Forms.Button btnDeposito;
        private System.Windows.Forms.Button Btnatrasplataforma;
    }
}