﻿namespace ProyectoBanco
{
    partial class frmClaveAleat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtclave3 = new System.Windows.Forms.TextBox();
            this.btnClave = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtclave3
            // 
            this.txtclave3.Location = new System.Drawing.Point(89, 66);
            this.txtclave3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtclave3.Name = "txtclave3";
            this.txtclave3.Size = new System.Drawing.Size(320, 22);
            this.txtclave3.TabIndex = 0;
            // 
            // btnClave
            // 
            this.btnClave.Location = new System.Drawing.Point(155, 98);
            this.btnClave.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnClave.Name = "btnClave";
            this.btnClave.Size = new System.Drawing.Size(191, 38);
            this.btnClave.TabIndex = 1;
            this.btnClave.Text = "Clave";
            this.btnClave.UseVisualStyleBackColor = true;
            this.btnClave.Click += new System.EventHandler(this.btnClave_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(151, 28);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(185, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "NUMERACION ALEATORIA:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // frmClaveAleat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 171);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClave);
            this.Controls.Add(this.txtclave3);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmClaveAleat";
            this.Text = "Datos";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtclave3;
        private System.Windows.Forms.Button btnClave;
        private System.Windows.Forms.Label label1;
    }
}