﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace reloj_checador
{
    public partial class Form1 : Form
    {
      
        DateTime datoEntrada = new DateTime();
        DateTime d = new DateTime();
        DateTime datoSalida = new DateTime();
        string Cb;

        bool C = false;
        bool CC = false;
        int Bandera = 0;

        public string Path = @"C:\Users\Cesar Basulto\desktop\Reloj Checador.txt";
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            Cb = comboBox1.Text;

            if (!File.Exists(Path))
            {
                using (StreamWriter sw = File.CreateText(Path))
                {
                    sw.WriteLine(Cb);
                    sw.WriteLine(datoEntrada);

                }
            }
            else
            {
                string append = "";
                if (Bandera == 1)
                {
                    append = "Entrada\n" + Cb + "\n" + datoEntrada + Environment.NewLine;
                }
                else 
                if (Bandera == 2)
                {
                    append = "Salida\n" + Cb + "\n" + datoSalida + Environment.NewLine;
                }

                File.AppendAllText(Path, append);
                datoEntrada = d;

            }


        }

        private void btnRevisar_Click(object sender, EventArgs e)
        {
            string readText = File.ReadAllText(Path);
            MessageBox.Show(readText);

        }

        private void rbtnEntrada_CheckedChanged(object sender, EventArgs e)
        {
            datoEntrada = dateTimePicker1.Value;
            btnRegistrar.Enabled = true;
            btnRevisar.Enabled = true;
            C = true;
            Bandera = 1;
        }

        private void rbtnSalida_CheckedChanged(object sender, EventArgs e)
        {
            datoSalida = dateTimePicker1.Value;
            btnRegistrar.Enabled = true;
            btnRevisar.Enabled = true;
            CC = true;
            Bandera = 2;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            datoEntrada = dateTimePicker1.Value;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
