﻿namespace CalculadoraOfertas
{
    partial class FormCalculadora
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlBotones = new System.Windows.Forms.Panel();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.btnSalir = new System.Windows.Forms.Button();
            this.pnlContenido = new System.Windows.Forms.Panel();
            this.lblSueldoNeto = new System.Windows.Forms.Label();
            this.lblPerseccionesTotal = new System.Windows.Forms.Label();
            this.lblDeduccionesTotal = new System.Windows.Forms.Label();
            this.lblIMSS = new System.Windows.Forms.Label();
            this.lblSubsidio = new System.Windows.Forms.Label();
            this.lblISR = new System.Windows.Forms.Label();
            this.lblDeducciones = new System.Windows.Forms.Label();
            this.lblImporteDos = new System.Windows.Forms.Label();
            this.lblImporte = new System.Windows.Forms.Label();
            this.lblSueldosSalarios = new System.Windows.Forms.Label();
            this.lblPercepciones = new System.Windows.Forms.Label();
            this.lblSueldoDiarioIntegrado = new System.Windows.Forms.Label();
            this.lblUMA = new System.Windows.Forms.Label();
            this.lblSueldoDiario = new System.Windows.Forms.Label();
            this.lblDias = new System.Windows.Forms.Label();
            this.txtDiasLaborados = new System.Windows.Forms.TextBox();
            this.txtSueldoDiario = new System.Windows.Forms.TextBox();
            this.txtSueldosSalarios = new System.Windows.Forms.TextBox();
            this.txtTotalPer = new System.Windows.Forms.TextBox();
            this.txtUMA = new System.Windows.Forms.TextBox();
            this.txtISR = new System.Windows.Forms.TextBox();
            this.txtSueldoDiarioIntegrado = new System.Windows.Forms.TextBox();
            this.txtSubsidio = new System.Windows.Forms.TextBox();
            this.txtCuotaIMSS = new System.Windows.Forms.TextBox();
            this.txtTotalDeducciones = new System.Windows.Forms.TextBox();
            this.txtSueldoNeto = new System.Windows.Forms.TextBox();
            this.btnGenerar = new System.Windows.Forms.Button();
            this.lblRestaSubsidio = new System.Windows.Forms.Label();
            this.pnlBotones.SuspendLayout();
            this.pnlContenido.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBotones
            // 
            this.pnlBotones.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.pnlBotones.Controls.Add(this.lblTitulo);
            this.pnlBotones.Controls.Add(this.btnSalir);
            this.pnlBotones.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBotones.Location = new System.Drawing.Point(0, 0);
            this.pnlBotones.Name = "pnlBotones";
            this.pnlBotones.Size = new System.Drawing.Size(600, 36);
            this.pnlBotones.TabIndex = 0;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.Location = new System.Drawing.Point(65, 4);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(337, 29);
            this.lblTitulo.TabIndex = 1;
            this.lblTitulo.Text = "Calculadora de ofertas laborales ";
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(566, 7);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(22, 23);
            this.btnSalir.TabIndex = 0;
            this.btnSalir.Text = "X";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // pnlContenido
            // 
            this.pnlContenido.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pnlContenido.Controls.Add(this.lblRestaSubsidio);
            this.pnlContenido.Controls.Add(this.btnGenerar);
            this.pnlContenido.Controls.Add(this.txtSueldoNeto);
            this.pnlContenido.Controls.Add(this.txtTotalDeducciones);
            this.pnlContenido.Controls.Add(this.txtCuotaIMSS);
            this.pnlContenido.Controls.Add(this.txtSubsidio);
            this.pnlContenido.Controls.Add(this.txtSueldoDiarioIntegrado);
            this.pnlContenido.Controls.Add(this.txtISR);
            this.pnlContenido.Controls.Add(this.txtUMA);
            this.pnlContenido.Controls.Add(this.txtTotalPer);
            this.pnlContenido.Controls.Add(this.txtSueldosSalarios);
            this.pnlContenido.Controls.Add(this.txtSueldoDiario);
            this.pnlContenido.Controls.Add(this.txtDiasLaborados);
            this.pnlContenido.Controls.Add(this.lblSueldoNeto);
            this.pnlContenido.Controls.Add(this.lblPerseccionesTotal);
            this.pnlContenido.Controls.Add(this.lblDeduccionesTotal);
            this.pnlContenido.Controls.Add(this.lblIMSS);
            this.pnlContenido.Controls.Add(this.lblSubsidio);
            this.pnlContenido.Controls.Add(this.lblISR);
            this.pnlContenido.Controls.Add(this.lblDeducciones);
            this.pnlContenido.Controls.Add(this.lblImporteDos);
            this.pnlContenido.Controls.Add(this.lblImporte);
            this.pnlContenido.Controls.Add(this.lblSueldosSalarios);
            this.pnlContenido.Controls.Add(this.lblPercepciones);
            this.pnlContenido.Controls.Add(this.lblSueldoDiarioIntegrado);
            this.pnlContenido.Controls.Add(this.lblUMA);
            this.pnlContenido.Controls.Add(this.lblSueldoDiario);
            this.pnlContenido.Controls.Add(this.lblDias);
            this.pnlContenido.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContenido.Location = new System.Drawing.Point(0, 36);
            this.pnlContenido.Name = "pnlContenido";
            this.pnlContenido.Size = new System.Drawing.Size(600, 314);
            this.pnlContenido.TabIndex = 1;
            // 
            // lblSueldoNeto
            // 
            this.lblSueldoNeto.AutoSize = true;
            this.lblSueldoNeto.Location = new System.Drawing.Point(368, 283);
            this.lblSueldoNeto.Name = "lblSueldoNeto";
            this.lblSueldoNeto.Size = new System.Drawing.Size(73, 13);
            this.lblSueldoNeto.TabIndex = 14;
            this.lblSueldoNeto.Text = "Sueldo NETO";
            // 
            // lblPerseccionesTotal
            // 
            this.lblPerseccionesTotal.AutoSize = true;
            this.lblPerseccionesTotal.Location = new System.Drawing.Point(18, 230);
            this.lblPerseccionesTotal.Name = "lblPerseccionesTotal";
            this.lblPerseccionesTotal.Size = new System.Drawing.Size(98, 13);
            this.lblPerseccionesTotal.TabIndex = 13;
            this.lblPerseccionesTotal.Text = "Total percepciones";
            // 
            // lblDeduccionesTotal
            // 
            this.lblDeduccionesTotal.AutoSize = true;
            this.lblDeduccionesTotal.Location = new System.Drawing.Point(316, 230);
            this.lblDeduccionesTotal.Name = "lblDeduccionesTotal";
            this.lblDeduccionesTotal.Size = new System.Drawing.Size(95, 13);
            this.lblDeduccionesTotal.TabIndex = 12;
            this.lblDeduccionesTotal.Text = "Total deducciones";
            // 
            // lblIMSS
            // 
            this.lblIMSS.AutoSize = true;
            this.lblIMSS.Location = new System.Drawing.Point(313, 200);
            this.lblIMSS.Name = "lblIMSS";
            this.lblIMSS.Size = new System.Drawing.Size(69, 13);
            this.lblIMSS.TabIndex = 11;
            this.lblIMSS.Text = "Cuota al imss";
            this.lblIMSS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSubsidio
            // 
            this.lblSubsidio.AutoSize = true;
            this.lblSubsidio.Location = new System.Drawing.Point(307, 161);
            this.lblSubsidio.Name = "lblSubsidio";
            this.lblSubsidio.Size = new System.Drawing.Size(95, 13);
            this.lblSubsidio.TabIndex = 10;
            this.lblSubsidio.Text = "Subsidio al empleo";
            this.lblSubsidio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblISR
            // 
            this.lblISR.AutoSize = true;
            this.lblISR.Location = new System.Drawing.Point(307, 125);
            this.lblISR.Name = "lblISR";
            this.lblISR.Size = new System.Drawing.Size(74, 13);
            this.lblISR.TabIndex = 9;
            this.lblISR.Text = "ISR calculado";
            this.lblISR.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDeducciones
            // 
            this.lblDeducciones.AutoSize = true;
            this.lblDeducciones.Location = new System.Drawing.Point(368, 90);
            this.lblDeducciones.Name = "lblDeducciones";
            this.lblDeducciones.Size = new System.Drawing.Size(70, 13);
            this.lblDeducciones.TabIndex = 8;
            this.lblDeducciones.Text = "Deducciones";
            // 
            // lblImporteDos
            // 
            this.lblImporteDos.AutoSize = true;
            this.lblImporteDos.Location = new System.Drawing.Point(516, 90);
            this.lblImporteDos.Name = "lblImporteDos";
            this.lblImporteDos.Size = new System.Drawing.Size(42, 13);
            this.lblImporteDos.TabIndex = 7;
            this.lblImporteDos.Text = "Importe";
            // 
            // lblImporte
            // 
            this.lblImporte.AutoSize = true;
            this.lblImporte.Location = new System.Drawing.Point(214, 90);
            this.lblImporte.Name = "lblImporte";
            this.lblImporte.Size = new System.Drawing.Size(42, 13);
            this.lblImporte.TabIndex = 6;
            this.lblImporte.Text = "Importe";
            // 
            // lblSueldosSalarios
            // 
            this.lblSueldosSalarios.AutoSize = true;
            this.lblSueldosSalarios.Location = new System.Drawing.Point(15, 125);
            this.lblSueldosSalarios.Name = "lblSueldosSalarios";
            this.lblSueldosSalarios.Size = new System.Drawing.Size(91, 13);
            this.lblSueldosSalarios.TabIndex = 5;
            this.lblSueldosSalarios.Text = "Sueldos y salarios";
            // 
            // lblPercepciones
            // 
            this.lblPercepciones.AutoSize = true;
            this.lblPercepciones.Location = new System.Drawing.Point(15, 90);
            this.lblPercepciones.Name = "lblPercepciones";
            this.lblPercepciones.Size = new System.Drawing.Size(72, 13);
            this.lblPercepciones.TabIndex = 4;
            this.lblPercepciones.Text = "Percepciones";
            // 
            // lblSueldoDiarioIntegrado
            // 
            this.lblSueldoDiarioIntegrado.AutoSize = true;
            this.lblSueldoDiarioIntegrado.Location = new System.Drawing.Point(292, 46);
            this.lblSueldoDiarioIntegrado.Name = "lblSueldoDiarioIntegrado";
            this.lblSueldoDiarioIntegrado.Size = new System.Drawing.Size(115, 13);
            this.lblSueldoDiarioIntegrado.TabIndex = 3;
            this.lblSueldoDiarioIntegrado.Text = "Sueldo diario integrado";
            // 
            // lblUMA
            // 
            this.lblUMA.AutoSize = true;
            this.lblUMA.Location = new System.Drawing.Point(289, 19);
            this.lblUMA.Name = "lblUMA";
            this.lblUMA.Size = new System.Drawing.Size(31, 13);
            this.lblUMA.TabIndex = 2;
            this.lblUMA.Text = "UMA";
            // 
            // lblSueldoDiario
            // 
            this.lblSueldoDiario.AutoSize = true;
            this.lblSueldoDiario.Location = new System.Drawing.Point(15, 46);
            this.lblSueldoDiario.Name = "lblSueldoDiario";
            this.lblSueldoDiario.Size = new System.Drawing.Size(68, 13);
            this.lblSueldoDiario.TabIndex = 1;
            this.lblSueldoDiario.Text = "Sueldo diario";
            // 
            // lblDias
            // 
            this.lblDias.AutoSize = true;
            this.lblDias.Location = new System.Drawing.Point(12, 19);
            this.lblDias.Name = "lblDias";
            this.lblDias.Size = new System.Drawing.Size(77, 13);
            this.lblDias.TabIndex = 0;
            this.lblDias.Text = "Dias laborados";
            // 
            // txtDiasLaborados
            // 
            this.txtDiasLaborados.Location = new System.Drawing.Point(156, 16);
            this.txtDiasLaborados.Name = "txtDiasLaborados";
            this.txtDiasLaborados.Size = new System.Drawing.Size(100, 20);
            this.txtDiasLaborados.TabIndex = 15;
            // 
            // txtSueldoDiario
            // 
            this.txtSueldoDiario.Location = new System.Drawing.Point(156, 43);
            this.txtSueldoDiario.Name = "txtSueldoDiario";
            this.txtSueldoDiario.Size = new System.Drawing.Size(100, 20);
            this.txtSueldoDiario.TabIndex = 19;
            // 
            // txtSueldosSalarios
            // 
            this.txtSueldosSalarios.Location = new System.Drawing.Point(156, 125);
            this.txtSueldosSalarios.Name = "txtSueldosSalarios";
            this.txtSueldosSalarios.Size = new System.Drawing.Size(100, 20);
            this.txtSueldosSalarios.TabIndex = 20;
            // 
            // txtTotalPer
            // 
            this.txtTotalPer.Location = new System.Drawing.Point(156, 227);
            this.txtTotalPer.Name = "txtTotalPer";
            this.txtTotalPer.Size = new System.Drawing.Size(100, 20);
            this.txtTotalPer.TabIndex = 21;
            // 
            // txtUMA
            // 
            this.txtUMA.Location = new System.Drawing.Point(458, 16);
            this.txtUMA.Name = "txtUMA";
            this.txtUMA.Size = new System.Drawing.Size(100, 20);
            this.txtUMA.TabIndex = 22;
            // 
            // txtISR
            // 
            this.txtISR.Location = new System.Drawing.Point(458, 125);
            this.txtISR.Name = "txtISR";
            this.txtISR.Size = new System.Drawing.Size(100, 20);
            this.txtISR.TabIndex = 23;
            // 
            // txtSueldoDiarioIntegrado
            // 
            this.txtSueldoDiarioIntegrado.Location = new System.Drawing.Point(458, 43);
            this.txtSueldoDiarioIntegrado.Name = "txtSueldoDiarioIntegrado";
            this.txtSueldoDiarioIntegrado.Size = new System.Drawing.Size(100, 20);
            this.txtSueldoDiarioIntegrado.TabIndex = 24;
            // 
            // txtSubsidio
            // 
            this.txtSubsidio.Location = new System.Drawing.Point(458, 158);
            this.txtSubsidio.Name = "txtSubsidio";
            this.txtSubsidio.Size = new System.Drawing.Size(100, 20);
            this.txtSubsidio.TabIndex = 25;
            // 
            // txtCuotaIMSS
            // 
            this.txtCuotaIMSS.Location = new System.Drawing.Point(458, 197);
            this.txtCuotaIMSS.Name = "txtCuotaIMSS";
            this.txtCuotaIMSS.Size = new System.Drawing.Size(100, 20);
            this.txtCuotaIMSS.TabIndex = 26;
            // 
            // txtTotalDeducciones
            // 
            this.txtTotalDeducciones.Location = new System.Drawing.Point(458, 227);
            this.txtTotalDeducciones.Name = "txtTotalDeducciones";
            this.txtTotalDeducciones.Size = new System.Drawing.Size(100, 20);
            this.txtTotalDeducciones.TabIndex = 27;
            // 
            // txtSueldoNeto
            // 
            this.txtSueldoNeto.Location = new System.Drawing.Point(458, 280);
            this.txtSueldoNeto.Name = "txtSueldoNeto";
            this.txtSueldoNeto.Size = new System.Drawing.Size(100, 20);
            this.txtSueldoNeto.TabIndex = 28;
            // 
            // btnGenerar
            // 
            this.btnGenerar.Location = new System.Drawing.Point(32, 283);
            this.btnGenerar.Name = "btnGenerar";
            this.btnGenerar.Size = new System.Drawing.Size(140, 23);
            this.btnGenerar.TabIndex = 29;
            this.btnGenerar.Text = "Generar Suelto NETO";
            this.btnGenerar.UseVisualStyleBackColor = true;
            this.btnGenerar.Click += new System.EventHandler(this.btnGenerar_Click);
            // 
            // lblRestaSubsidio
            // 
            this.lblRestaSubsidio.AutoSize = true;
            this.lblRestaSubsidio.Location = new System.Drawing.Point(442, 161);
            this.lblRestaSubsidio.Name = "lblRestaSubsidio";
            this.lblRestaSubsidio.Size = new System.Drawing.Size(10, 13);
            this.lblRestaSubsidio.TabIndex = 30;
            this.lblRestaSubsidio.Text = "-";
            // 
            // FormCalculadora
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 350);
            this.Controls.Add(this.pnlContenido);
            this.Controls.Add(this.pnlBotones);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormCalculadora";
            this.Text = "Form1";
            this.pnlBotones.ResumeLayout(false);
            this.pnlBotones.PerformLayout();
            this.pnlContenido.ResumeLayout(false);
            this.pnlContenido.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlBotones;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Panel pnlContenido;
        private System.Windows.Forms.Label lblSueldosSalarios;
        private System.Windows.Forms.Label lblPercepciones;
        private System.Windows.Forms.Label lblSueldoDiarioIntegrado;
        private System.Windows.Forms.Label lblUMA;
        private System.Windows.Forms.Label lblSueldoDiario;
        private System.Windows.Forms.Label lblDias;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Label lblSueldoNeto;
        private System.Windows.Forms.Label lblPerseccionesTotal;
        private System.Windows.Forms.Label lblDeduccionesTotal;
        private System.Windows.Forms.Label lblIMSS;
        private System.Windows.Forms.Label lblSubsidio;
        private System.Windows.Forms.Label lblISR;
        private System.Windows.Forms.Label lblDeducciones;
        private System.Windows.Forms.Label lblImporteDos;
        private System.Windows.Forms.Label lblImporte;
        private System.Windows.Forms.TextBox txtSueldoNeto;
        private System.Windows.Forms.TextBox txtTotalDeducciones;
        private System.Windows.Forms.TextBox txtCuotaIMSS;
        private System.Windows.Forms.TextBox txtSubsidio;
        private System.Windows.Forms.TextBox txtSueldoDiarioIntegrado;
        private System.Windows.Forms.TextBox txtISR;
        private System.Windows.Forms.TextBox txtUMA;
        private System.Windows.Forms.TextBox txtTotalPer;
        private System.Windows.Forms.TextBox txtSueldosSalarios;
        private System.Windows.Forms.TextBox txtSueldoDiario;
        private System.Windows.Forms.TextBox txtDiasLaborados;
        private System.Windows.Forms.Button btnGenerar;
        private System.Windows.Forms.Label lblRestaSubsidio;
    }
}

