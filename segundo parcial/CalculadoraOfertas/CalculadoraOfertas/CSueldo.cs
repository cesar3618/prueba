﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculadoraOfertas
{
    class CSueldo
    {
        private double Salario ;
        private double diasTrabajo ;
        private int iterador;
        private const  double UMA = 86.88;
        private double[] LimInferior =
        {
            0.01 , 19.04, 161.53, 283.87, 329.98, 395.07, 796.80, 1255.86, 2397.63, 3196.83, 9590.47
        };
        private double[] LimSuperior =
        {
            19.03, 161.52, 283.86, 329.97, 395.06, 796.79, 1255.85, 2397.62 , 3196.82 , 9590.46 , 10000000
        };
        private double[] CuotaFija =
        {
            0, 0.37 , 9.48 , 22.79 , 30.17, 41.84 , 127.65 , 235.62 , 578.15 , 833.89 , 3007.73
        };
        private double[] porcentajeLimInferior =
        {
            1.92 , 6.40 ,  10.88 , 16.00 , 17.92 ,  21.36 , 23.52 , 30.00 , 32.00 , 34.00 , 35.00
        };
        //arreglos para el calculo del subsidio por empleo
        private double[] LimInferiorSubsidio =
        {
            0.01 ,  58.20 ,  87.29 ,  114.25 , 116.39 ,  146.26 , 155.18 , 175.52 ,  204.77, 234.02, 242.85
        };
        private double[] LimSuperiorSubsidio =
        {
            58.19 , 87.28 ,  114.24 ,  116.38 ,  146.25 ,  155.17 , 175.51 ,  204.76 ,  234.01 , 242.84 , 100000000000
        };

        private double[] SubsidioTrabajo =
        {
            13.39 ,  13.38 ,  13.38 ,  12.92 ,  12.58 ,  11.65 ,  10.69 ,  9.69 ,  8.34 ,  7.16 , 0.00
        };

        private  bool encontrarISR()
        {
            for(int i = 0; i< LimInferior.Length; i++)
            {
                if(LimInferior[i]<Salario && Salario<LimSuperior[i])
                {
                    iterador = i;
                    return true;
                }
            }
            return false;
        }

        public double ISR()
        {
            if (encontrarISR())
            {
                double ExcedenteLim = Salario - LimInferior[iterador];
                double tasa = porcentajeLimInferior[iterador];
                double impuestoMarginal = (tasa * ExcedenteLim) / 100;
                double cuotaFija = CuotaFija[iterador];
                return Math.Round((impuestoMarginal + cuotaFija) * diasTrabajo,2);
            }
            else
            {
                return 0;
            }
              
        }

        private  bool encontrarSubsidio()
        {
            for (int i = 0; i < LimInferiorSubsidio.Length; i++)
            {
                if ( LimInferiorSubsidio[i]<Salario && Salario < LimSuperiorSubsidio[i])
                {
                    iterador = i;
                    return true;
                }
            }
            return false;
        }

        public double Subsidio()
        {
            if(encontrarSubsidio())
            {
                double subsidioTrabajo = SubsidioTrabajo[iterador];
                return Math.Round( subsidioTrabajo * diasTrabajo, 3);
            }
            else
            {
                return 0;
            }

        }

        public double encontrarSDI()
        {
            double año = 365;
            double vacaciones = 6;
            double primaVacacional = 0.25;
            double aguinaldo = 15;

            double sdi = (primaVacacional * vacaciones);
            sdi = sdi + año + aguinaldo;

            double factorIntegrante = sdi / año;

            sdi = Salario * factorIntegrante;

            return Math.Round(sdi,3);
        }

        public double encontrarIMSS()
        {
            double Excedente = 0;
            double prestacionesDinero = 0;
            double prestacionesEspecie = 0;
            double invalidezVida = 0;
            double cesantiaVejez = 0;
            double tem = UMA * 3;
            double res;
            if(tem< encontrarSDI())
            {
                res = encontrarSDI() - tem;
                Excedente = (res * diasTrabajo * 0.400) / 100;
            }
            else
            {
                Excedente = 0;
            }

            prestacionesDinero = (encontrarSDI() * diasTrabajo * 0.250) / 100;
            prestacionesEspecie = (encontrarSDI() * diasTrabajo * 0.375) / 100;
            invalidezVida = (encontrarSDI() * diasTrabajo * 0.625) / 100;
            cesantiaVejez = (encontrarSDI() * diasTrabajo * 1.125) / 100;

            res = Excedente + prestacionesDinero + prestacionesEspecie + invalidezVida + cesantiaVejez;

            return Math.Round(res,3);
        }

        public double getUMA()
        {
            return UMA;
        }

        public double getSueldo()
        {
            return diasTrabajo * Salario;
        }
       
       public void  setSalario(double a)
        {
            if(a<=0)
            {
                this.Salario = 0;
            }
            else
            {
                this.Salario = a;
            }
        }

        public void setDias(double a)
        {
            if (a<1)
            {
                this.diasTrabajo = 1;
            }
            else
            {
                this.diasTrabajo = a;
            }
        }


    }
}
